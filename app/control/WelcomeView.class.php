<?php


class WelcomeView extends TPage
{
    private $form;

    function __construct()
    {
        parent::__construct();

        $this->form = new BootstrapFormBuilder();


        $nome = new TEntry('nome');


        $this->form->addFields([new TLabel('Nome: ')], [$nome]);

        $this->form->addAction('Salvar Curso', new TAction(array($this, 'onSave')), 'fa:check-circle-o green');

        parent::add($this->form);

        // add the template to the page
    }

    function onSave()


    {
        try {
            TTransaction::open('sample');
            // $object = $this->form->getData('CursoModel');
            $object->store();
            TTransaction::close();
        } catch (Exeption $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }
}