<?php

use Adianti\Database\TCriteria;
use Adianti\Database\TRepository;
use Adianti\Database\TTransaction;
use Adianti\Widget\Container\TPanelGroup;
use Adianti\Widget\Datagrid\TPageNavigation;
use Adianti\Widget\Dialog\TMessage;
use Adianti\Widget\Form\TLabel;
use Adianti\Widget\Datagrid\TDataGrid;
use Adianti\Wrapper\BootstrapDatagridWrapper;

class TransporteList extends TPage
{
  private $datagrid;
  private $pageNavigation;
  function __construct()
  {
    parent::__construct();

    $this->datagrid = new BootstrapDatagridWrapper(new  TDataGrid);

    // create the datagrid columns
    $id       = new TDataGridColumn('id',    'ID',    'center', '10%');
    $nome       = new TDataGridColumn('nome',    'Nome',    'left',   '30%');
    $id->title = 'Código';
    $nome->title = 'Nome';

    $this->datagrid->addColumn($id);
    $this->datagrid->addColumn($nome);


    // creates two datagrid actions
    $btnEdit = new TDataGridAction(array('TransporteForm', 'onEdit'));
    $btnEdit->setUseButton(TRUE);
    $btnEdit->setButtonClass('btn btn-default');
    $btnEdit->setLabel('Editar');
    $btnEdit->setImage('fa:edit blue');
    $btnEdit->setFields(['id', 'nome']);

    $btnDelete = new TDataGridAction(array($this, 'onDelete'));
    $btnDelete->setUseButton(TRUE);
    $btnDelete->setButtonClass('btn btn-default');
    $btnDelete->setLabel('Delete');
    $btnDelete->setImage('fa:trash red');
    $btnDelete->setField('id');


    // add the actions to the datagrid
    $this->datagrid->addAction($btnEdit);
    $this->datagrid->addAction($btnDelete);


    // creates the datagrid model
    $this->datagrid->createModel();


    // Navegação das páginas'
    $this->pageNavigation = new TPageNavigation;
    $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
    $this->pageNavigation->setWidth($this->datagrid->getWidth());




    // $btn = $this->form->addAction('Salvar', new TAction(array($this, 'onSend')), 'fa:save white');
    // $btn->class = 'btn btn-sm btn-primary';
    // $this->form->addAction('Novo', new TAction(array('TransporteForm', 'onSend')), 'fa:eraser red');


    // wrap the page content using vertical box
    $vbox = new TVBox;
    $vbox->style = 'width: 100%';
    $vbox->add(new TXMLBreadCrumb('menu.xml', __CLASS__));

    $vbox->add(TPanelGroup::pack("", $this->datagrid, $this->pageNavigation));


    parent::add($vbox);
  }


  function onReload($param = null)
  {
    try {

      TTransaction::open('sample');

      $repository = new TRepository('TransporteModel');
      $limit = 10;
      $criteria = new TCriteria;
      $criteria->setProperties($param);
      $criteria->setProperty("limit", $limit);
      $data = $repository->load($criteria);

      $this->datagrid->clear();

      if ($data) {
        foreach ($data as $d) {
          $this->datagrid->addItem($d);
        }
      }

      $criteria->resetProperties();
      $count = $repository->count($criteria);
      $this->pageNavigation->setCount($count);
      $this->pageNavigation->setProperties($param);
      $this->pageNavigation->setLimit($limit);


      TTransaction::close();
    } catch (Exeption $e) {
      new TMessage('error', $e->getMessage());
    }
  }


  // funções ------------------------------------------------


  public function onDelete($param)
  {
    // define the delete action
    $action = new TAction(array($this, 'Delete'));
    $action->setParameters($param); // pass the key parameter ahead

    new TQuestion(('Deseja realmente apagar?'), $action);
  }

  function Delete($param)
  {
    try {
      $key = $param['key']; // get the parameter $key
      TTransaction::open('sample'); // open transaction 
      $data = new TransporteModel($key, FALSE);
      $data->delete(); // delete object
      TTransaction::close(); // close transaction

      $this->onReload($param); // reload the listing
      new TMessage('info', AdiantiCoreTranslator::translate('Record deleted')); // success message
    } catch (Exeption $e) {
      new TMessage('error', $e->getMessage());
    }
  }

  function onView($param)
  {
    $id = $param['id'];
    $nome = $param['nome'];
    new TMessage('info', "The ID is: <b>$id</b> <br> The name is : <b>$nome</b>");
  }

  /**
   * shows the page
   */
  function show()
  {
    $this->onReload();
    parent::show();
  }




  public function onEdit($param)
  {
    try {

      $key = $param['key'];
      TTransaction::open('sample');

      $data = new TransporteModel($key, FALSE);
      $this->form->setData($data);

      TTransaction::close();


      $this->onReload($param);


      new TMessage('info', "Atualizado com sucesso!");
    } catch (Exeption $e) {
      new TMessage('error', $e->getMessage());
    }
  }
}