<?php

use Adianti\Control\TAction;
use Adianti\Database\TCriteria;
use Adianti\Database\TRepository;
use Adianti\Database\TTransaction;
use Adianti\Widget\Dialog\TMessage;
use Adianti\Widget\Form\TLabel;

class TransporteForm extends TPage
{
  private $form;
  private $novoCadastro;

  function __construct()
  {
    parent::__construct();

    // create the form
    $this->form = new BootstrapFormBuilder;
    $this->form->setFormTitle('Cadastro transporte');
    $this->form->class = 'tform';


    // create the form fields
    $id          = new TEntry('id');
    $nome = new TEntry('nome');


    $this->form->addFields([new TLabel('Nome: ')], [$nome]);
    $id->setEditable(FALSE);
    $this->form->addFields([$id]);


    $nome->placeholder = 'Digite um nome';
    $nome->setTip('Nome do veículo');
    $id->{'style'} = 'display: none';


    // define the form action 
    $btn = $this->form->addAction('Salvar', new TAction(array($this, 'onSend')), 'fa:save white');
    $btn->class = 'btn btn-sm btn-primary';
    $this->form->addAction('Novo', new TAction(array('TransporteForm', 'onClear')), 'fa:eraser red');


    parent::add($this->form);
  }




  // funções ------------------------------------------------
  /**
   * Clear form data
   * @param $param Request
   */
  public function onClear($param)
  {
    $this->form->clear();
    $this->novoCadastro = true;
  }

  public function onSend()
  {
    try {
      $data = $this->form->getData('TransporteModel');

      if ($data->nome != '') {

        TTransaction::open('sample');

        $data->store();
        $this->form->setData($data);

        $pageList = new TAction(array('TransporteList', 'onReload'));
        new TMessage('info', 'Salvo com sucesso', $pageList);
      } else {
        new TMessage('info', "Sem dados");
      }

      TTransaction::close();
    } catch (Exeption $e) {
      new TMessage('error', $e->getMessage());
      TTransaction::rollback();
    }
  }



  function onView($param)
  {
    $id = $param['id'];
    $nome = $param['nome'];
    new TMessage('info', "The ID is: <b>$id</b> <br> The name is : <b>$nome</b>");
  }

  /**
   * shows the page
   */
  function show()
  {
    parent::show();
  }




  public function onEdit($param)
  {
    try {

      $key = $param['key'];
      TTransaction::open('sample');


      $data = new TransporteModel($key, FALSE);

      $novoCadastro = $this->novoCadastro = true;

      if ($novoCadastro == true) {
        $this->form->setData($data);
      } else {
        $this->$novoCadastro = false;
      }

      TTransaction::close();
    } catch (Exeption $e) {
      new TMessage('error', $e->getMessage());
    }
  }
}