<?php

use Adianti\Database\TRecord;

class TransporteModel extends TRecord
{
  const TABLENAME = 'tb_curso';
  const PRIMARYKEY = 'id';
  const IDPOLICY = 'max';

  public function __construct($id = NULL)
  {
    parent::__construct($id);
    parent::addAttribute('nome');
  }
}